module PayPal
  class Order
    attr_accessor :env
    attr_reader :order_id, :amount, :currency

    def initialize(client_id:, client_secret:, order_id: "", env: "SANDBOX")
      @client_id = client_id
      @client_secret = client_secret
      if env == "SANDBOX"
        @base_url = "https://api.sandbox.paypal.com"
      else
        @base_url = "https://api.paypal.com"
      end
      auth_token_resp = generate_auth_token
      @access_token =  auth_token_resp["access_token"]
      @token_exp = auth_token_resp["expires_in"]
    end

    def generate_auth_token
      key = Base64.encode64("<#{@client_id}:#{@client_secret}>")

      request_token = Typhoeus::Request.post(
        headers: {
          "Accept": "application/x-www-form-urlencoded",
          "Content-Type": "application/json",
          "Acccept-Language": "en_US",
          "Auhtorization": "Basic #{key}"
        },
        params: {
          "grant_type": "client_credentials"
        }
      )
      JSON.parse(request_token.body)
    end

    def create(amount:, payer:, currency: "USD")
      json_body = {
        intent: "CAPTURE",
        payer: payer,
        purchase_units: {
          currency: currency,
          amount: amount
        }
      }
      capture_payment = Typhoeus::Request.post(
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer #{@access_token}"
        },
        body: JSON.dump(json_body)
      )
      JSON.parse(capture_payment.body)
    end

    def show_details
    end

    def authorize
    end
  end
end