module Mailgun
  API_KEY = ""
  API_TOKEN = ""
  class Email
    attr_reader :domain, :key
    attr_accessor :api_url, :to, :from, :subject, :html_body

    def initialize(domain:, key:)
      @domain = domain
      @key = Base64.encode64("api:#{key}")
      @api_url = "https://api.mailgun.net/v3/#{@domain}/messages"
    end

    def format_plaintxt(file:)
    end

    def send(to:, from:, subject:, html_body:)
      @to = to
      @from = from
      @subject = subject
      @html_body = html_body
      send_email = Typhoeus::Request.post(@api_url,
        headers: {"Authorization": "Basic #{@key}"},
        params: {
          from: from,
          to: to,
          subject: subject,
          text: "Test",
          html: html_body.to_s
      })
        send_email.body
    end
  end
end