// show shipping address form if it has been disabled
function disableForm(button: HTMLButtonElement, toggleButton: HTMLButtonElement, formID: string) : void {
  const inputs =          <NodeListOf<HTMLInputElement>>document.querySelectorAll(`#${formID} input`);
  const selects =         <NodeListOf<HTMLSelectElement>>document.querySelectorAll(`#${formID} select`);
  const shippingToggle =  document.getElementById("shippingToggle");
  const shippingDetails = document.getElementById("shippingDetails");
  button.addEventListener("click", () => {
    for (let i = 0; i < inputs.length; i++) {
      if (inputs[i].value.length !== 0 || inputs[i].value !== " ") {
        inputs[i].disabled = true;
      }
    }
    for (let i = 0; i < selects[i].length; i++)  {
      if (selects[i].selectedIndex !== 1) {
        selects[i].disabled = true;
      }
    }

    toggleButton.style.display = "inline";
    button.style.display = "none";
    shippingToggle.style.display = "none";
    shippingDetails.style.display = "block";
  });
}

// show shipping address form if it has been disabled
function enableForm(button: HTMLButtonElement, toggleButton: HTMLButtonElement, formID: string) : void {
  const inputs = <NodeListOf<HTMLInputElement>>document.querySelectorAll(`#${formID} input`);
  const selects = <NodeListOf<HTMLSelectElement>>document.querySelectorAll(`#${formID} select`);
  const shippingToggle = document.getElementById("shippingToggle");
  const shippingDetails = document.getElementById("shippingDetails");

  button.addEventListener("click", () => {
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].disabled = false;
    }
    for (let i = 0; i < selects[i].length; i++)  {
      if (i === 1) {
        continue;
      } else {
        selects[i].disabled = false;
      }
    }

    toggleButton.style.display = "inline";
    button.style.display = "none";
    shippingToggle.style.display = "block";
    shippingDetails.style.display = "none";

  })
}

// shipping address display code
const displayEmail =      document.getElementById("displayEmail");
const shipAddressOneDis = document.getElementById("shipAddressOneDis");
const shipAddressTwoDis = document.getElementById("shipAddressTwoDis");
const shipZipCodeDis =    document.getElementById("shipZipCodeDis");
const shipStateDis =      document.getElementById("shipStateDis");
const shipCityDis =       document.getElementById("shipCityDis");
const shipFirstNameDis =  document.getElementById("shipFirstNameDis");
const shipLastNameDis =   document.getElementById("shipLastNameDis");

// shipping address form fields
const emailAddress =    <HTMLInputElement>document.getElementById("emailAddress");
const shipFirstName =   <HTMLInputElement>document.getElementById("shipFirstName");
const shipLastName =    <HTMLInputElement>document.getElementById("shipLastName");
const shipAddressOne =  <HTMLInputElement>document.getElementById("shipAddressOne");
const shipAddressTwo =  <HTMLInputElement>document.getElementById("shipAddressTwo");
const shipCity =        <HTMLInputElement>document.getElementById("shipCity");
const shipZipCode =     <HTMLInputElement>document.getElementById("shipZipCode");
const shipState =       <HTMLSelectElement>document.getElementById("shipState");

const saveShipping =      <HTMLButtonElement>document.getElementById("saveShipping");
const editShipping =      <HTMLButtonElement>document.getElementById("editShipping");
const shippingInfoForm =  <HTMLFormElement>document.getElementById("shippingInfoForm");


shippingInfoForm.addEventListener("submit", (event: Event) => { event.preventDefault(); });
disableForm(saveShipping, editShipping, "shippingInfoForm");
enableForm(editShipping, saveShipping, "shippingInfoForm");

saveShipping.addEventListener("click", () => {
  displayEmail.textContent =      emailAddress.value;
  shipFirstNameDis.textContent =  shipFirstName.value;
  shipLastNameDis.textContent =   shipLastName.value;
  shipAddressOneDis.textContent = shipAddressOne.value;
  shipAddressTwoDis.textContent = shipAddressTwo.value;
  shipCityDis.textContent =       shipCity.value;
  shipStateDis.textContent =      shipState.options[shipState.selectedIndex].text;
  shipZipCodeDis.textContent =    shipZipCode.value;
});

const selectItemQty = <HTMLSelectElement>document.getElementById("selectItemQty");
const inputItemQty =  <HTMLInputElement>document.getElementById("inputItemQty");

selectItemQty.addEventListener("change", (event: Event) => {
  const currentEl = <HTMLSelectElement>event.currentTarget;
  const currentValue = currentEl.options[currentEl.selectedIndex].value;
});

inputItemQty.addEventListener("change", (event: Event) => {

});

function confirmationButton(event: Event, data, orderDetails) : any {

}

declare var paypal;

document.addEventListener("DOMContentLoaded", () => {
  // PayPal button code
const paypalButton = document.getElementById("paypalButton");
  if (paypalButton !== null) {
    const cartTotal = document.getElementById("cartTotal").getAttribute("data-cart-total");
    paypal.Buttons({
      style: {
        layout: "vertical",
        color: "gold",
        shape: "rect",
        label: "pay"
      },
      createOrder: function(data, actions) {
        return actions.order.create({
          purchase_units: [{
            amount: {
              currency: "USD",
              value: cartTotal
            }
          }]
        });
      },
      onApprove: function(data, actions) {
        return actions.order.capture().then(function(details) {
            // shipping address form fields
          const emailAddress =      <HTMLInputElement>document.getElementById("emailAddress");
          const shipFirstName =     <HTMLInputElement>document.getElementById("shipFirstName");
          const shipLastName =      <HTMLInputElement>document.getElementById("shipLastName");
          const shipAddressOne =    <HTMLInputElement>document.getElementById("shipAddressOne");
          const shipAddressTwo =    <HTMLInputElement>document.getElementById("shipAddressTwo");
          const shipCity =          <HTMLInputElement>document.getElementById("shipCity");
          const shipZipCode =       <HTMLInputElement>document.getElementById("shipZipCode");
          const shipState =         <HTMLSelectElement>document.getElementById("shipState");
          const cartTotal = document.getElementById("cartTotal");
          const existingCartItem = document.getElementById("cartItem");

          return fetch("/orders", {
            method: "post",
            headers: {
              "content-type": "application/json"
            },
            body: JSON.stringify({
              payer: details.payer,
              total_amount: parseFloat(cartTotal.textContent).toFixed(2),
              order_details_id: existingCartItem.getAttribute("data-cart-id"),
              paypal_order_id: data.orderID,
              email: emailAddress.value,
              first_name: shipFirstName.value,
              last_name: shipLastName.value,
              address_one: shipAddressOne.value,
              address_two: shipAddressTwo.value,
              city: shipCity.value,
              zip_code: shipZipCode.value,
              state: shipState.value,
              country_code: "US"
            })
          }).then((resp) => {
            window.location.replace(window.location.origin + "/orders/thank-you");
          });
        });
      }
    }).render("#paypalButton");
  }
})
