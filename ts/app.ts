import {HTTP} from "./http";
const mockButton = document.getElementById("mockButton");
if (mockButton !== null) {
  mockButton.addEventListener("click", () => {
    HTTP.post("/products", "").then((data) => {
      console.log(data)
    })
  });
}

function showPopupMsg(type: string, msg: string) {
  const body = document.querySelector("body");
  const popup = document.createElement("div");

  popup.id = "popupMsg";
  if (type === "success") {
    popup.classList.add("popup-success");
  } else if (type === "not_found") {
    
  } else {
    popup.classList.add("popup-error");
  }
  popup.textContent = msg;
  body.appendChild(popup);
  setTimeout(() => {
    const popupMsg = document.getElementById("popupMsg");
    popupMsg.classList.add("popup-fadeout");
  }, 3000);
  setTimeout(() => {
    const popupMsg = document.getElementById("popupMsg");
    popupMsg.remove();
  }, 2000);
}

const cartChangeIcon = document.getElementById("cartChangeIcon");
const cartEmpty = document.getElementById("emptyCart");
const cartViewTotal = document.getElementById("cartViewTotal");

// cart actions
const viewCart = document.getElementById("viewCart");
const shoppingCartView = document.getElementById("shoppingCartView");

// show/hide cart
viewCart.addEventListener("click", () => {
  const isVisibile = (shoppingCartView.style.display === "none") ? true : false;

  if (isVisibile) {
    shoppingCartView.style.display = "initial";
  } else {
    shoppingCartView.style.display = "none";
  }

  cartChangeIcon.style.display = "none"; 
});

function createCart(form: HTMLFormElement, container: HTMLElement) {
  const numCartItems = new FormData(form);
  const existingCartItem = document.getElementById("cartItem");
    if (existingCartItem === null) {
      setTimeout(() => {}, 500);

      HTTP.post("/carts", numCartItems).then((resp) => {
        const json = JSON.parse(resp as string);
        const cartItem = document.createElement("div");
        const pureG = document.createElement("div");
        const cartImg = document.createElement("div");
        const productImg = document.createElement("img");
        const cartProductName = document.createElement("div");
        const cartQtyContainter = document.createElement("div");
        const cartNumItems = document.createElement("div");

        cartItem.id = "cartItem"; // parent
        cartItem.setAttribute("data-cart-id", json.id)
        pureG.className = "pure-g"; // grid parent
        cartImg.className = "pure-u-1-5"; //mimg container
        // img element
        productImg.className = "pure-img";
        productImg.src = "/img/single-block.png";
        // grid children
        cartProductName.className = "pure-u-3-5";
        cartQtyContainter.className = "pure-u-1-5";
        cartNumItems.className = "cart-num-items"; // qty element

        // build the cart
        for (let i = 0; i < 2; i++) {
          const p = document.createElement("p");
          if (i === 1) {
            const span = document.createElement("span");
            span.id = "removeItems";
            span.textContent = "Remove";
            // add an event to not only remove the element 
            // but also delete the cart when created
            span.addEventListener("click", () => {
              const cartItem = document.getElementById("cartItem");
              cartViewTotal.textContent = "0.00";
              cartItem.remove();
              HTTP.delete(`/carts/${json.id}`, "").then((resp) => {
                showPopupMsg("success", "Cart successful emptied!");
              })
              cartEmpty.style.display = "block";
            });
            p.appendChild(span);
          } else {
            p.textContent = "Trunk Blocks";
          }
          cartProductName.appendChild(p);
        }

        for (let i = 0; i < 2; i++) {
          const span = document.createElement("span");
          if (i === 1) {
            span.id = "cartItemQty";
            span.textContent = parseInt(json.quantity).toString();
            cartViewTotal.textContent = json.subtotal;
          } else {
            span.textContent = "Qty: ";
          }


          cartNumItems.appendChild(span);
        }
        cartImg.appendChild(productImg);
        cartQtyContainter.appendChild(cartNumItems);
        pureG.appendChild(cartImg);
        pureG.appendChild(cartProductName);
        pureG.appendChild(cartQtyContainter);
        cartItem.appendChild(pureG);
        container.appendChild(cartItem);
        cartChangeIcon.style.display = "block";
      });
    } else {
      const cartID = existingCartItem.getAttribute("data-cart-id");
      HTTP.patch(`carts/${cartID}`, numCartItems).then((resp) => {
        const json = JSON.parse(resp as string);
        const cartItemQty = document.getElementById("cartItemQty");
        const productQty = parseInt(json.quantity)
        cartViewTotal.textContent = parseFloat(json.subtotal).toFixed(2);
        cartItemQty.textContent = productQty.toString();
        cartChangeIcon.style.display = "block";
      })

    }

}

const removeItems = document.getElementById("removeItems")
if (removeItems !== null) {
  removeItems.addEventListener("click", () => {
    const cartItem = document.getElementById("cartItem");
    const cartID = cartItem.getAttribute("data-cart-id");
    const data = new FormData();
    HTTP.delete(`/carts/${cartID}`, "").then((resp) => {
    })
    cartViewTotal.textContent = "0.00";
    cartItem.remove();
    cartEmpty.style.display = "block";
  });
}



const addCartItem = <HTMLFormElement>document.getElementById("addCartItem");
if (addCartItem !== null) {
  addCartItem.addEventListener("submit", (event: Event) => {
    event.preventDefault();
    const cartContainer = document.getElementById("cartContainer");
    createCart(addCartItem, cartContainer);
    cartEmpty.style.display = "none";
  });
} 