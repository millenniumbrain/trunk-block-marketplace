Sequel.migration do
  up do
    create_table(:guest_users) do
      primary_key :id, type: :Bignum
      String :uuid
      String :email
      String :fname
      String :lname
      DateTime :created_at
      DateTime :updated_at
    end
  end
end