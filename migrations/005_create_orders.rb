Sequel.migration do
  up do
    create_table(:orders) do
      primary_key :id, type: :Bignum
      String :uuid
      String :paypal_order_id
      Integer :status_code
      BigDecimal :total
      DateTime :created_at
      DateTime :updated_at
      foreign_key :user_id
      foreign_key :guest_user_id
    end
  end
end