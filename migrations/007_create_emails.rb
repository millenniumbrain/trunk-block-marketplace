Sequel.migration do
  up do
    create_table(:emails) do
      primary_key :id, type: :Bignum
      String :uuid
      String :to
      String :from
      String :subject
      String :body, text: true
      String :html_body, text: true
      DateTime :created_at
      DateTime :updated_at
      foreign_key :guest_user_id
    end
  end
end