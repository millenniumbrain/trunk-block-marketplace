Sequel.migration do
  up do
    create_table(:order_details) do
      primary_key :id, type: :Bignum
      String :uuid
      Integer :quantity
      BigDecimal :subtotal
      DateTime :created_at
      DateTime :updated_at
      foreign_key :order_id
      foreign_key :product_id
    end
  end
end