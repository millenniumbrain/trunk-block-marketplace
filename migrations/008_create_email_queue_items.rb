Sequel.migration do
  up do
    create_table(:email_queue_items) do
      primary_key :id, type: :Bignum
      String :uuid
      Integer :status
      DateTime :created_at
      DateTime :updated_at
      foreign_key :email_id
    end
  end
end