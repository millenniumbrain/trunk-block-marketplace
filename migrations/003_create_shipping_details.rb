Sequel.migration do
  up do
    create_table(:shipping_details) do
      primary_key :id, type: :Bignum
      String :uuid
      String :address_line_one
      String :address_line_two
      String :city
      String :admin_area_two # state or provence
      String :admin_area_one # country
      String :postal_code # postal code or zip code
      foreign_key :user_id
      foreign_key :guest_user_id
    end
  end
end