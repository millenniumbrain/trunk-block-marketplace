Sequel.migration do
  up do
    create_table(:products) do
      primary_key :id, type: :Bignum
      String :uuid
      String :name
      String :sku
      String :img
      String :img_thumbnail
      String :description
      String :soft_descriptor
      BigDecimal :price
      DateTime :created_at
      DateTime :updated_at
    end
  end
end