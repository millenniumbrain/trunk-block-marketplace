<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <p>Hello&nbsp;<span id="firstName"></span>&nbsp;<span id="lastName"></span>,</p>
        
Thanks for ordering your brand new Trunk Blocks!
Your order has been placed and when your order is ready you should receive an update from trunkblock@gmail.com

Your Shipping Information
First Name: {first_name}
Last Name: {last_name}
Address Line 1: {address_one}
Address Line 2: {address_two}
City: {city}
Zip Code: {zip_code}
State: {state}
Country: {country}

