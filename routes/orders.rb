App.route("orders") do |r|
  r.is do
    r.get do
    end

    r.post do
      confirmed_order = JSON.parse(r.body.read)
      pp confirmed_order
      cart = OrderDetail[confirmed_order["order_details_id"].to_i]
      order = Order[cart.order_id]
      order.paypal_order_id = confirmed_order["paypal_order_id"]
      new_guest = GuestUser.new do |g|
        g.fname = confirmed_order["first_name"]
        g.lname = confirmed_order["last_name"]
        g.email = confirmed_order["email"]
      end
      new_guest.save

      shipping = ShippingDetail.new do |s|
        s.address_line_one = confirmed_order["address_one"]
        s.address_line_two = confirmed_order["address_two"]
        s.city = confirmed_order["city"]
        s.admin_area_two = confirmed_order["state"] # state or provence
        s.admin_area_one = confirmed_order["country_code"] # country
        s.postal_code = confirmed_order["zip_code"] # postal code or zip code
        s.guest_user_id = new_guest.id
      end
      shipping.save

      order_confirmation = Mailgun::Email.new(
        domain: ENV["MAILGUN_DOMAIN"],
        key: ENV["MAILGUN_KEY"]
      )

      # build confirmation email
      confirm_html = Nokogiri::HTML(File.open("templates/order-confirmation.html"))
      confirm_html.at_css("span#fName").content = new_guest.fname
      confirm_html.at_css("span#lName").content = new_guest.lname
      confirm_html.at_css("span#addressOne").content = shipping.address_line_one
      confirm_html.at_css("span#addressTwo").content = shipping.address_line_two
      confirm_html.at_css("span#city").content = shipping.city
      confirm_html.at_css("span#zipCode").content = shipping.postal_code
      confirm_html.at_css("span#state").content = shipping.admin_area_two
      confirm_html.at_css("span#country").content = shipping.admin_area_one

      confirmed_order = Mailgun::Email.new(
        domain: ENV["MAILGUN_DOMAIN"],
        key: ENV["MAILGUN_KEY"]
      )
      confirmed_order.send(
        to: new_guest.email,
        from: "No Reply <support@#{confirmed_order.domain}>",
        subject: "Trunk Block - Order Confirmation!",
        html_body: confirm_html.to_s
      )
      ""
    end
  end

  r.is "thank-you" do
    r.get do
      session[:shop_token] = nil
      view("thank-you")
    end
  end
end