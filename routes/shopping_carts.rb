App.route("carts") do |r|

  r.on Integer do |id|
    r.get do
      order_detail = OrderDetail[id]
      order_detail.to_hash.to_json
    end

    r.patch do
      response["Content-Type"] = "applicaiton/json"
      add_item = r.params

      order_details = {}
      if session[:shop_token] != nil
        old_order_details = OrderDetail[id]
        item_qty = add_item["item_qty"].to_i 
        old_order_details.quantity += item_qty 
        pp old_order_details.subtotal
        new_subtotal = old_order_details.subtotal
        old_order_details.save
        order_details["subtotal"] = "%0.2f" % new_subtotal
        order_details = old_order_details.to_hash

        session[:shop_token] = order_details.to_json
        order_details.to_json
      end
      order_details.to_json
    end


    r.delete do
      response['Content-Type'] = "application/json"
      deleted_order = {}
      if session[:shop_token] != nil
        order_details = OrderDetail[id]
        order = Order[order_details.order_id]
        order_details.delete
        order.delete
        session[:shop_token] = nil
        {msg: "Your cart was successfully emptied"}.to_json
      else
        {msg: "Nothing to delete"}
      end
    end
  end

  r.is do
    # return an empty json object and 500 status
    # 500: {}
    # return an empty json object if there is no shop token
    #
    # or return the current order detail amount
    # 200: {
    #   quantity: integer,
    #   total: double,
    #   subtotal: double,
    #   product_id: integer
    # }
    r.get do
      session[:shop_token] = nil
      if session[:shop_token] != nil
        session[:shop_token] = nil
      else
        {msg: "Shop token is empty"}.to_json
      end
    end

    # create an encoded session of the user's order details
    # return an empty json object and 500 status
    # 500: {}
    # or return the current order detail amount
    # 200: {
    #   quantity: integer,
    #   total: double,
    #   subtotal: double,
    #   product_id: integer
    # }
    r.post do
      response["Content-Type"] = "applicaiton/json"
      add_item = r.params

      order_details = "{}"

      if session[:shop_token] == nil
        product = Product[1]

        new_order = Order.create
        new_order_details = OrderDetail.new do |o|
          o.quantity = add_item["item_qty"].to_i
          o.product_id = product.id
          o.order_id  = new_order.id
        end
        pp new_order_details.subtotal
        new_subtotal = new_order_details.subtotal
        new_order_details.save
        order_details = new_order_details.to_hash
        order_details[:subtotal] = "%0.2f" % new_subtotal
        session[:shop_token] = order_details.to_json

        order_details.to_json
      else
      end

    end
  end
end