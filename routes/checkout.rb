App.route("checkout") do |r|
  r.is do
    r.post do
      GuestUser.create(
        email: "test@email.com",
        fname: "William",
        lname: "Wilson",
        
      )
    end

    r.get do
      view("checkout")
    end
  end

  r.is "shopping-carts" do
  end
end