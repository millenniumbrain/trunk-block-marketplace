App.route("products") do |r|
  r.is do
    r.get do
      response['Content-Type'] = "application/json"
      product = Product.limit(1)
      product = product.to_a.map(&:to_hash)
      product.to_json
    end

    r.post do
      product = Product.new do |p|
        p.name = "Trunk Blocks"
        p.price = 30.00
      end
      product.save
      product.to_hash.to_json
    end
  end

  r.is Integer do |product_id|
  end
end