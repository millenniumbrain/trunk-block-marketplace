class GuestUser < Sequel::Model(:guest_users)
  def before_create
    self.uuid ||= SecureRandom.uuid.gsub("-", "").upcase
    super
  end
end