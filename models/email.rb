class Email < Sequel::Model(:emails)
  many_to_one :email_queue
  many_to_one :guest_user
end