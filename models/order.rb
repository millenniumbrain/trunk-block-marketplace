class Order < Sequel::Model(:orders)
  many_to_one :user
  one_to_many :order_details
  one_to_many :shopping_carts

  def before_create
    self.uuid ||= SecureRandom.uuid.gsub("-", "").upcase
    super
  end
end