class OrderDetail < Sequel::Model(:order_details)
  one_to_many :product
  one_to_many :order

  def subtotal
    price = Product[self.product_id].price
    self.subtotal = price * self.quantity
  end

  def before_create
    self.uuid ||= SecureRandom.uuid.gsub("-", "").upcase
    super
  end
end