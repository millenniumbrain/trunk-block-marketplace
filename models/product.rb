class Product < Sequel::Model(:products)
  one_to_many :order_details

  def before_create
    self.uuid ||= SecureRandom.uuid.gsub("-", "").upcase
    super
  end
end