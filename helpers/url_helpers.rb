def base_url
  "#{request.env['rack.url_scheme']}://#{request.env['HTTP_HOST']}"
end