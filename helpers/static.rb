def link_js(filename)
  "#{base_url}/js/#{filename}.js" 
end

def link_css(filename)
  "#{base_url}/css/#{filename}.css"
end

def link_img(filename, ext: "jpg")
  "#{base_url}/img/#{filename}.#{ext}"
end